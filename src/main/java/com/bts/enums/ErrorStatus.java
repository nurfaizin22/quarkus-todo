package com.bts.enums;

public enum ErrorStatus {
    Entity_NOT_FOUND(400, "", "Entity Not found")
    ;

    private Integer statusCode;
    private String message;
    private String errorMessage;

    ErrorStatus(Integer statusCode, String message, String errorMessage) {
        this.statusCode = statusCode;
        this.message = message;
        this.errorMessage = errorMessage;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}