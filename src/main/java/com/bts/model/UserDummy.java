package com.bts.model;

import java.util.Collections;
import java.util.Set;

public class UserDummy {

    public String username;
    public String password;
    public Set<Role> roles;

    public UserDummy(String username, String password, Set<Role> roles) {
        this.username = username;
        this.password = password;
        this.roles = roles;
    }

    public static UserDummy findByUsername(String username) {

        //if using Panache pattern (extends or PanacheEntity PanacheEntityBase)
        //return find("username", username).firstResult();

        String userUsername = "user";

        //generated from password encoder
        String userPassword = "cBrlgyL2GI2GINuLUUwgojITuIufFycpLG4490dhGtY=";

        String adminUsername = "admin";

        //generated from password encoder
        String adminPassword = "dQNjUIMorJb8Ubj2+wVGYp6eAeYkdekqAcnYp+aRq5w=";

        if (username.equals(userUsername)) {
            return new UserDummy(userUsername, userPassword, Collections.singleton(Role.USER));
        } else if (username.equals(adminUsername)) {
            return new UserDummy(adminUsername, adminPassword, Collections.singleton(Role.ADMIN));
        } else {
            return null;
        }
    }
}
