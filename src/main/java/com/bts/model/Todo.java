package com.bts.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "todos")
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    private boolean completed;

    @OneToMany(mappedBy = "todo")
    private Set<TodoUser> todoUsers;

    public Set<TodoUser> getTodoUsers() {
        return todoUsers;
    }

    public void setTodoUsers(Set<TodoUser> todoUsers) {
        this.todoUsers = todoUsers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
