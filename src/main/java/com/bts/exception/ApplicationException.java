package com.bts.exception;

import java.io.Serializable;

public class ApplicationException extends Exception implements Serializable {

    private static final long serialVersionUID = 1L;

    public ApplicationException() {
    }

    public ApplicationException(String message) {
        super(message);
    }

    public ApplicationException(String message, Exception e) {
        super(message, e);
    }
}
