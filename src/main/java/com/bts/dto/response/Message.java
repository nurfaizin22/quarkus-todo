package com.bts.dto.response;

public class Message {

    public String content;

    public Message(String content) {
        this.content = content;
    }
}
