package com.bts.dto.response;

import java.time.LocalDateTime;

public class TodoDetailResponse {

    private Long id;

    private String taskName;


    private LocalDateTime createdAt;

    private LocalDateTime dueDate;

    private boolean late;

    private boolean completed;

    public TodoDetailResponse()  {

    }

    public TodoDetailResponse(Long id, String taskName, LocalDateTime createdAt, LocalDateTime dueDate, boolean late, boolean completed) {
        this.id = id;
        this.taskName = taskName;
        this.createdAt = createdAt;
        this.dueDate = dueDate;
        this.late = late;
        this.completed = completed;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isLate() {
        return late;
    }

    public void setLate(boolean late) {
        this.late = late;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
