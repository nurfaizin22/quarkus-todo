package com.bts.dto.response;

public class UserResponse {

    private Long id;

    private String name;

    private String username;

    public UserResponse() {

    }

    public UserResponse(Long id, String name, String username) {
        this.id = id;
        this.name = name;
        this.username = username;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
