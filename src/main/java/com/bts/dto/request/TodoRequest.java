package com.bts.dto.request;

import javax.validation.constraints.NotBlank;
import java.util.List;

public class TodoRequest {

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    private boolean completed;

    private List<Long> userid;

    public List<Long> getUserid() {
        return userid;
    }

    public void setUserid(List<Long> userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
