package com.bts.util.mapper;

import com.bts.dto.response.UserResponse;
import com.bts.model.User;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserMapper {

    public UserResponse convertResponse(User user) {
        return new UserResponse(user.getId(), user.getName(), user.getUsername());
    }
}
