package com.bts.util.mapper;

import com.bts.dto.response.TodoDetailResponse;
import com.bts.dto.response.TodoResponse;
import com.bts.model.Todo;
import com.bts.model.TodoDetail;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class TodoMapper {

    public TodoResponse convertTodo(Todo todo) {
        return new TodoResponse(
                todo.getId(),
                todo.getName(),
                todo.getDescription(),
                todo.isCompleted()
        );
    }

    public TodoDetailResponse convertTodoDetail(TodoDetail todoDetail) {
        return  new TodoDetailResponse(
                todoDetail.getId(),
                todoDetail.getTaskName(),
                todoDetail.getCreatedAt(),
                todoDetail.getDueDate(),
                todoDetail.isLate(),
                todoDetail.isCompleted()
        );
    }
}
