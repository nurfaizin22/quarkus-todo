package com.bts.service;

import com.bts.dto.request.AuthRequest;
import com.bts.dto.response.AuthResponse;

public interface AuthService {

    AuthResponse auth(AuthRequest request);
}
