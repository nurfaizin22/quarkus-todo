package com.bts.service.impl;

import com.bts.dto.request.AuthRequest;
import com.bts.dto.response.AuthResponse;
import com.bts.model.Role;
import com.bts.model.User;
import com.bts.repository.UserRepository;
import com.bts.security.PasswordEncoder;
import com.bts.security.TokenUtils;
import com.bts.service.AuthService;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.HashSet;
import java.util.Set;

@RequestScoped
public class AuthServiceImpl implements AuthService {


    @Inject
    PasswordEncoder passwordEncoder;

    @Inject
    UserRepository userRepository;

    @ConfigProperty(name = "com.bts.todo.jwt.duration") public Long duration;
    @ConfigProperty(name = "mp.jwt.verify.issuer") public String issuer;

    @Override
    public AuthResponse auth(AuthRequest request) {

        User user = userRepository.findByUsername(request.username).orElseThrow();
        Set<Role> roles = new HashSet<>();
        roles.add(Role.USER);

        if (user != null && user.getPassword().equals(passwordEncoder.encode(request.password))) {
            try {
                String token = TokenUtils.generateToken(user.getUsername(), roles, duration, issuer);
                return new AuthResponse(token);
            } catch (Exception e) {
                 throw new NullPointerException();
            }
        } else {
            throw new NullPointerException();
        }

    }
}
