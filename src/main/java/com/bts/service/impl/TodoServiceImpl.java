package com.bts.service.impl;

import com.bts.dto.request.TodoRequest;
import com.bts.dto.response.TodoResponse;
import com.bts.model.Todo;
import com.bts.model.TodoUser;
import com.bts.model.User;
import com.bts.repository.TodoRepository;
import com.bts.repository.TodoUserRepository;
import com.bts.repository.UserRepository;
import com.bts.service.TodoService;
import com.bts.util.mapper.TodoMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class TodoServiceImpl implements TodoService {

    @Inject
    TodoRepository todoRepository;

    @Inject
    UserRepository userRepository;

    @Inject
    TodoUserRepository todoUserRepository;

    @Inject
    TodoMapper mapper;

    @Transactional
    @Override
    public TodoResponse createTodo(TodoRequest request) {
        Todo todo = new Todo();
        todo.setName(request.getName());
        todo.setDescription(request.getDescription());
        todo.setCompleted(request.isCompleted());

        todoRepository.save(todo);

        for(Long id : request.getUserid()) {
            User user = userRepository.findById(id).orElseThrow();

            TodoUser todoUser = new TodoUser();
            todoUser.setUser(user);
            todoUser.setTodo(todo);
            todoUserRepository.save(todoUser);
        }

        return mapper.convertTodo(todo);
    }

    @Override
    public TodoResponse updateTodo(TodoRequest request, Long id) {
        Todo todo = todoRepository.findById(id).orElseThrow();

        todo.setName(request.getName());
        todo.setDescription(request.getDescription());
        todo.setCompleted(request.isCompleted());

        todoRepository.save(todo);

        return mapper.convertTodo(todo);
    }

    @Override
    public TodoResponse getTodo(Long id) {
        Todo todo = todoRepository.findById(id).orElseThrow();

        return mapper.convertTodo(todo);
    }

    @Override
    public void deleteTodo(Long id) {
        Todo todo = todoRepository.findById(id).orElseThrow();

        todoRepository.delete(todo);
    }

    @Override
    public List<TodoResponse> listTodo() {
        List<Todo> todos = todoRepository.findAll();
        System.out.println(todos.size());
        List<TodoResponse> responses = todos.stream().map(mapper::convertTodo).collect(Collectors.toList());

        return responses;
    }


}
