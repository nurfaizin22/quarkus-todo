package com.bts.service.impl;

import com.bts.dto.request.UserRequest;
import com.bts.dto.response.UserResponse;
import com.bts.model.User;
import com.bts.repository.UserRepository;
import com.bts.security.PasswordEncoder;
import com.bts.service.UserService;
import com.bts.util.mapper.UserMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class UserServiceImpl implements UserService {

    @Inject
    UserRepository userRepository;

    @Inject
    UserMapper mapper;

    @Inject
    PasswordEncoder passwordEncoder;

    @Transactional
    @Override
    public UserResponse createUser(UserRequest request) {

        User user = new User();
        user.setName(request.getName());
        user.setUsername(request.getUsername());
        user.setPassword(passwordEncoder.encode(request.getPassword()));

        userRepository.save(user);

        return mapper.convertResponse(user);
    }

    @Transactional
    @Override
    public UserResponse updateUser(UserRequest request, Long id) {

        User user = userRepository.findById(id).orElseThrow();
        user.setName(request.getName());

        userRepository.save(user);

        return mapper.convertResponse(user);
    }

    @Override
    public UserResponse getUser(Long id) {

        User user = userRepository.findById(id).orElseThrow();

        return mapper.convertResponse(user);
    }

    @Transactional
    @Override
    public void deleteUser(Long id) {
        User user = userRepository.findById(id).orElseThrow();

        userRepository.delete(user);
    }

    @Override
    public List<UserResponse> listUser() {
        List<User> users = userRepository.findAll();
        List<UserResponse> responses = users.stream()
                .map(mapper::convertResponse).collect(Collectors.toList());

        return responses;
    }

    @Override
    public UserResponse getByUsername(String username) {

        User user = userRepository.findByUsername(username).orElseThrow();

        return mapper.convertResponse(user);
    }
}
