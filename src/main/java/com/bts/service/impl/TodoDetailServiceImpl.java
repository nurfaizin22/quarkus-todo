package com.bts.service.impl;

import com.bts.dto.request.TodoDetailRequest;
import com.bts.dto.response.TodoDetailResponse;
import com.bts.model.TodoDetail;
import com.bts.repository.TodoDetailRepository;
import com.bts.service.TodoDetailService;
import com.bts.util.mapper.TodoMapper;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class TodoDetailServiceImpl implements TodoDetailService {

    @Inject
    TodoDetailRepository repository;

    @Inject
    TodoMapper mapper;

    @Override
    public TodoDetailResponse createTodoDetail(TodoDetailRequest request) {
        TodoDetail todoDetail = new TodoDetail();
        todoDetail.setTaskName(request.getTaskName());
        todoDetail.setCreatedAt(LocalDateTime.now());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(request.getDueDate(), formatter);
        todoDetail.setDueDate(dateTime);

        repository.save(todoDetail);

        return mapper.convertTodoDetail(todoDetail);
    }

    @Override
    public TodoDetailResponse updateTodoDetail(TodoDetailRequest request, Long id) {

        TodoDetail todoDetail = repository.findById(id).orElseThrow();

        todoDetail.setTaskName(request.getTaskName());
        todoDetail.setCreatedAt(LocalDateTime.now());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(request.getDueDate(), formatter);
        todoDetail.setDueDate(dateTime);

        repository.save(todoDetail);

        return mapper.convertTodoDetail(todoDetail);
    }

    @Override
    public TodoDetailResponse getTodoDetail(Long id) {
        TodoDetail todoDetail = repository.findById(id).orElseThrow();

        return mapper.convertTodoDetail(todoDetail);
    }

    @Override
    public void deleteTodoDetail(Long id) {
        TodoDetail todoDetail = repository.findById(id).orElseThrow();

        repository.delete(todoDetail);

    }

    @Override
    public List<TodoDetailResponse> listTodoDetail() {
        List<TodoDetail> todoDetails = repository.findAll();

        List<TodoDetailResponse> responses = todoDetails.stream().map(mapper::convertTodoDetail).collect(Collectors.toList());

        return responses;
    }
}
