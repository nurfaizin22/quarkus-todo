package com.bts.service;

import com.bts.dto.request.TodoRequest;
import com.bts.dto.response.TodoResponse;

import java.util.List;

public interface TodoService {

    TodoResponse createTodo(TodoRequest request);

    TodoResponse updateTodo(TodoRequest request, Long id);

    TodoResponse getTodo(Long id);

    void deleteTodo(Long id);

    List<TodoResponse> listTodo();
}
