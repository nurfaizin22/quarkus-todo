package com.bts.service;

import com.bts.dto.request.TodoDetailRequest;
import com.bts.dto.response.TodoDetailResponse;

import java.util.List;

public interface TodoDetailService {

    TodoDetailResponse createTodoDetail(TodoDetailRequest request);

    TodoDetailResponse updateTodoDetail(TodoDetailRequest request, Long id);

    TodoDetailResponse getTodoDetail(Long id);

    void deleteTodoDetail(Long id);

    List<TodoDetailResponse> listTodoDetail();
}
