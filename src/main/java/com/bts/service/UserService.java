package com.bts.service;

import com.bts.dto.request.UserRequest;
import com.bts.dto.response.UserResponse;

import java.util.List;

public interface UserService {

    UserResponse createUser(UserRequest request);

    UserResponse updateUser(UserRequest request, Long id);

    UserResponse getUser(Long id);

    void deleteUser(Long id);

    List<UserResponse> listUser();

    UserResponse getByUsername(String username);
}
