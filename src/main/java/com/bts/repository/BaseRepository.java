package com.bts.repository;

import java.util.List;
import java.util.Optional;

public interface BaseRepository<T, ID> {

    T save(T entity);

    Optional<T> findById(ID id);

    void delete(T entity);

    List<T> findAll();
}
