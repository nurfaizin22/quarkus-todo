package com.bts.repository;


import com.bts.model.Todo;


public interface TodoRepository extends BaseRepository<Todo, Long>{

}
