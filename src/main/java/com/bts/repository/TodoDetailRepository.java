package com.bts.repository;


import com.bts.dto.request.TodoDetailRequest;
import com.bts.model.TodoDetail;

import java.util.List;

public interface TodoDetailRepository extends BaseRepository<TodoDetail, Long> {

    List<TodoDetail> findByTodoId(Long id);
}
