package com.bts.repository;

import com.bts.model.TodoUser;

public interface TodoUserRepository extends BaseRepository<TodoUser, Long> {
}
