package com.bts.repository;

import com.bts.model.User;
import io.quarkus.vertx.web.Param;

import java.util.Optional;

public interface UserRepository extends BaseRepository<User, Long> {

    Optional<User> findByUsername(@Param("username") String username);
}
