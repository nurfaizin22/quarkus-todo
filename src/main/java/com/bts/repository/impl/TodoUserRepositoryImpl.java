package com.bts.repository.impl;

import com.bts.model.TodoUser;
import com.bts.repository.TodoUserRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class TodoUserRepositoryImpl implements TodoUserRepository {

    @Inject
    EntityManager entityManager;

    @Override
    public TodoUser save(TodoUser entity) {
        entityManager.persist(entity);

        return entity;
    }

    @Override
    public Optional<TodoUser> findById(Long aLong) {
        TodoUser todoUser = entityManager.find(TodoUser.class, aLong);
        return Optional.of(todoUser);
    }

    @Override
    public void delete(TodoUser entity) {
        entityManager.remove(entity);
    }

    @Override
    public List<TodoUser> findAll() {
        return entityManager.createQuery("SELECT t FROM TodoUser t", TodoUser.class).getResultList();
    }
}
