package com.bts.repository.impl;

import com.bts.dto.request.TodoRequest;
import com.bts.model.Todo;
import com.bts.repository.TodoRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class TodoRepositoryImpl implements TodoRepository {

    @Inject
    EntityManager entityManager;


    @Override
    @Transactional
    public Todo save(Todo todo) {

        entityManager.persist(todo);

        return todo;
    }

    @Override
    public Optional<Todo> findById(Long id) {
        Todo todo = entityManager.find(Todo.class, id);
        return Optional.of(todo);
    }


    @Override
    public void delete(Todo todo) {
        entityManager.remove(todo);
    }



    @Override
    public List<Todo> findAll() {
        return entityManager.createQuery("SELECT t FROM Todo t", Todo.class).getResultList();
    }
}
