package com.bts.repository.impl;

import com.bts.model.TodoDetail;
import com.bts.repository.TodoDetailRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class TodoDetailRepositoryImpl implements TodoDetailRepository {

    @Inject
    EntityManager entityManager;

    @Override
    public TodoDetail save(TodoDetail todoDetail) {

        entityManager.persist(todoDetail);
        return todoDetail;
    }


    @Override
    public Optional<TodoDetail> findById(Long aLong) {
        TodoDetail todoDetail = entityManager.find(TodoDetail.class, aLong);

        return Optional.of(todoDetail);
    }

    @Override
    public void delete(TodoDetail entity) {
        entityManager.remove(entity);
    }


    @Override
    public List<TodoDetail> findAll() {
        return entityManager.createQuery("SELECT t FROM TodoDetail  t", TodoDetail.class).getResultList();
    }

    @Override
    public List<TodoDetail> findByTodoId(Long id) {

        return entityManager.createQuery("SELECT t from TodoDetail t WHERE t.todo.id=?1").getResultList();
    }
}
