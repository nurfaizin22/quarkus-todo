package com.bts.repository.impl;

import com.bts.model.User;
import com.bts.repository.UserRepository;
import io.smallrye.common.constraint.Assert;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class UserRepositoryImpl implements UserRepository {

    @Inject
    EntityManager entityManager;

    @Override
    public User save(User entity) {

        Assert.assertNotNull(entity);

        entityManager.persist(entity);

        return entity;
    }

    @Override
    public Optional<User> findById(Long aLong) {
        User user = entityManager.find(User.class, aLong);
        return Optional.of(user);
    }

    @Override
    public void delete(User entity) {
        Assert.assertNotNull(entity);

        entityManager.remove(entity);
    }

    @Override
    public List<User> findAll() {
        return entityManager.createQuery("SELECT u FROM User  u", User.class).getResultList();
    }

    @Override
    public Optional<User> findByUsername(String username) {
        Query query = entityManager.createQuery("SELECT u FROM User u WHERE u.username = :username");
        query.setParameter("username", username);

        User user = (User) query.getSingleResult();
        return Optional.ofNullable(user);
    }
}
