package com.bts.controller;

import com.bts.base.BaseResponse;
import com.bts.dto.request.UserRequest;
import com.bts.dto.response.UserResponse;
import com.bts.service.UserService;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserController {

    @Inject
    UserService userService;

    @PermitAll
    @POST
    public Response createUser(UserRequest request) {
        UserResponse data = userService.createUser(request);
        BaseResponse<UserResponse> response = new BaseResponse<>();
        response.setStatusCode(200);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(data);

        return Response.ok(response).build();
    }

    @PUT
    @Path("/{id}")
    public Response updateUser(UserRequest request, @PathParam("id") Long id) {
        UserResponse data = userService.updateUser(request, id);
        BaseResponse<UserResponse> response = new BaseResponse<>();
        response.setStatusCode(200);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(data);

        return Response.ok(response).build();
    }

    @GET
    @Path("/{id}")
    public Response getUser( @PathParam("id") Long id) {
        UserResponse data = userService.getUser( id);
        BaseResponse<UserResponse> response = new BaseResponse<>();
        response.setStatusCode(200);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(data);

        return Response.ok(response).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteUser( @PathParam("id") Long id) {
        userService.deleteUser(id);
        BaseResponse<UserResponse> response = new BaseResponse<>();
        response.setStatusCode(200);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(null);

        return Response.ok(response).build();
    }

}
