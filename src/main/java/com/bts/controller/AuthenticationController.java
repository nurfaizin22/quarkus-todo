package com.bts.controller;

import com.bts.dto.request.AuthRequest;
import com.bts.dto.response.AuthResponse;
import com.bts.service.AuthService;

import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AuthenticationController {


    @Inject
    AuthService authService;

    @PermitAll
    @POST
    @Path("/login")
    public Response login(AuthRequest authRequest) {

        AuthResponse authResponse = authService.auth(authRequest);

        return Response.ok(authResponse).build();

    }

}
