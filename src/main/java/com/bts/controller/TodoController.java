package com.bts.controller;

import com.bts.base.BaseResponse;
import com.bts.dto.request.TodoRequest;
import com.bts.dto.response.TodoResponse;
import com.bts.service.TodoService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/todo")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TodoController {

    @Inject
    TodoService todoService;

    @POST
    public Response createTodo(TodoRequest request) {
        TodoResponse data = todoService.createTodo(request);
        BaseResponse<TodoResponse> response = new BaseResponse<>();
        response.setStatusCode(200);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(data);


        return Response.ok(response).build();
    }

    @PUT
    @Path("/{id}")
    public Response updateTodo(TodoRequest request,@PathParam("id") Long id) {
        TodoResponse data = todoService.updateTodo(request, id);
        BaseResponse<TodoResponse> response = new BaseResponse<>();
        response.setStatusCode(200);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(data);


        return Response.ok(response).build();
    }

    @GET
    @Path("/{id}")
    public Response getTodo(@PathParam("id") Long id) {
        TodoResponse data = todoService.getTodo(id);
        BaseResponse<TodoResponse> response = new BaseResponse<>();
        response.setStatusCode(200);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(data);


        return Response.ok(response).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteTodo(@PathParam("id") Long id) {
        todoService.deleteTodo(id);
        BaseResponse<TodoResponse> response = new BaseResponse<>();
        response.setStatusCode(200);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(null);


        return Response.ok(response).build();
    }

    @GET
    public Response getAllTodo() {
        List<TodoResponse> data = todoService.listTodo();
        BaseResponse<List<TodoResponse>> response = new BaseResponse<>();
        response.setStatusCode(200);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(data);


        return Response.ok(response).build();
    }

}
