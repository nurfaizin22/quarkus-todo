package com.bts.controller;

import com.bts.base.BaseResponse;
import com.bts.dto.request.TodoDetailRequest;
import com.bts.dto.response.TodoDetailResponse;
import com.bts.service.TodoDetailService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/todo-detail")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TodoDetailController {

    @Inject
    TodoDetailService todoDetailService;

    @POST
    public Response createTodoDetail(TodoDetailRequest request) {

        TodoDetailResponse data = todoDetailService.createTodoDetail(request);
        BaseResponse<TodoDetailResponse> response = new BaseResponse<>();
        response.setStatusCode(201);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(data);

        return Response.ok(response).build();
    }

    @PUT
    @Path("/{id}")
    public Response updateTodoDetail(TodoDetailRequest request, @PathParam("id") Long id) {
        TodoDetailResponse data = todoDetailService.updateTodoDetail(request, id);
        BaseResponse<TodoDetailResponse> response = new BaseResponse<>();
        response.setStatusCode(201);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(data);

        return Response.ok(response).build();
    }

    @GET
    @Path("/{id}")
    public Response getTodoDetail(@PathParam("id") Long id) {
        TodoDetailResponse data = todoDetailService.getTodoDetail(id);
        BaseResponse<TodoDetailResponse> response = new BaseResponse<>();
        response.setStatusCode(201);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(data);

        return Response.ok(response).build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteTodoDetail(@PathParam("id") Long id) {
        todoDetailService.deleteTodoDetail(id);
        BaseResponse<TodoDetailResponse> response = new BaseResponse<>();
        response.setStatusCode(201);
        response.setMessage("Success");
        response.setErrorMessages("");
        response.setData(null);

        return Response.ok(response).build();
    }

}
