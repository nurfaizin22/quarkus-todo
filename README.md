# todo project

# Requirement
- Java 11
- Postgresql
- Hibernate

# How to run
- create file `application.properties`
- copy config from `application.properties.example`
- Modify `application.properties` according to your environment

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

